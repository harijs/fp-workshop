import getRightAngles from "./getRightAngles";

describe("rightAngles", () => {
  it("should be defined", () => {
    expect(getRightAngles).toBeDefined();
  });

  it("it should return 52 triplets of right angle triangles", () => {
    const result = getRightAngles(100);
    expect(result).toMatchSnapshot();
  });
});
