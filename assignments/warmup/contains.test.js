import contains from "./contains";

describe("contains", () => {
  const myObj = {
    somekey: ["foo", "bar"],
    otherkey: "baz",
    another: 7,
    bool: false
  };

  it("it should find 'foo'", () => {
    const result = contains("foo")(myObj);
    expect(result).toBe(true);
  });

  it("it should not find 'foobar'", () => {
    const result = contains("foobar")(myObj);
    expect(result).toBe(false);
  });

  it("it should find 'bar'", () => {
    const result = contains("bar")(myObj);
    expect(result).toBe(true);
  });

  it("it should find false", () => {
    const result = contains(false)(myObj);
    expect(result).toBe(true);
  });

  it("it should find 7", () => {
    const result = contains(7)(myObj);
    expect(result).toBe(true);
  });

  it("it should not find true", () => {
    const result = contains(true)(myObj);
    expect(result).toBe(false);
  });
});
