# Composition

In this assignment we're going to create a lot of small functions which will be used to compose bigger functions (compositions). 
The idea is to show how modular and small the functions can be and how easy it is to read the flow of the application.
All functions are covered in the tests and they must all pass in order to compose the final composition  `getAverageAgeInCity`.

It is important to understand why `Array.prototype` functions such as `map` and `filter` are wrapped inside another function in order to play well when composing functions. Notice that the order of the arguments is flipped too: the first argument is the function that is used to "filter" or "map" on the items. This will return another function which can be used to apply on arrays. The reason that the actual data is the last argument is because it can be used to compose other functions.

Libraries such as Ramda provide utility functions like `R.map` and `R.filter`, but we're going to write our own.

## filter

This function accepts a function that acts as a predicate. It returns a function that accepts an array which can be filtered. This returns the filtered array.

`filter :: (a -> Boolean) -> Array a -> Array a`

## map

This function accepts a function that acts as a the 'transformer'. It returns a function that accepts an array which can be mapped over. This returns the mapped array.

`map :: (a -> b) -> Array a -> Array b`

## prop

This function accepts a string that acts as the key selector on an object. It returns a function that accepts object that returns the value of the key.

`prop :: String -> Person -> a`

## equals

This function accepts a value that is strictly matched against the second argument.

`equals :: a -> b -> Boolean`

## getCity

This function accepts a Person object and returns the city. You can use the `prop` for this.

`getCity :: Person -> String`

## getAge

This function accepts a Person object and returns the age. You can use the `prop` for this.

`getAge :: Person -> Number`

## isAmsterdam

This function accepts a string and returns true if it matches Amsterdam. You can use the `equals` for this.

`isAmsterdam :: String -> Boolean`

## getAverageNumber

This function accepts an array of numbers and returns the average value.

`getAverageNumber :: Array Number -> Number`

But we're going to make this one a little bit harder by using the "Fork" (join) combinator.
The Fork combinator takes three functions and finally a value. It takes the value and passes it do the second and third function. The result of those functions are then passed into the first function to combine the results. The result of that function will be returned as the final value.

The type signature is as folllows:
`fork :: Function -> (Function -> a) -> (Function -> a) -> a -> b`

Remember that (to make everything a bit more tricky) every function is curried, so even the join function in this combinator.

## personLivesInAmsterdam

This function accepts a person and returns true if the person lives in Amsterdam.

`personLivesInAmsterdam :: Person -> Boolean`

This function can be composed out of two other functions that are compatible in type signature notation.
You can use `R.compose` (`import R from 'ramda'`) to compose this function.

In the following order (_note that with `compose` it in reverse_!), you can see that each return value is compatible with the input of the next.
```
|> getCity :: Person -> String
|> isAmsterdam :: String -> Boolean
```

## filterOnPeopleFromAmsterdam

This function accepts an array of Person objects and returns only people that live in Amsterdam.

`filterOnPeopleFromAmsterdam :: Array Person -> Array Person`

## getAverageAgeInAmsterdam

This function accepts a list of Person objects and returns the average age of the people that live in Amsterdam.
You can use `R.compose` (`import R from 'ramda'`) to compose this function.

`getAverageAgeInAmsterdam :: Array Person -> Number`

Again, you can see that each return value is compatible with the input of the next:

```
|> map :: Array Person -> Array Person
|> filterOnPeopleFromAmsterdam :: Array Person -> Array Number
|> getAverageNumber :: Array Number -> Number
```
