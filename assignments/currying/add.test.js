import add from "./add";

describe("add", () => {
  it("should add the number three", () => {
    const addThree = add(3);
    const result = addThree(4);

    expect(result).toBe(7);
  });
});
