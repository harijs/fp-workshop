# Transducer

## getNamesWithInitials

`getNamesWithInitials :: Array String -> Array String -> Array String`

### Hints

- You may use the R.transduce function https://ramdajs.com/docs/#transduce