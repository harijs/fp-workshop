import { identity, Wrapper } from "./functor";

describe("identity", () => {
  it("should return itself", () => {
    const result = identity(5);
    expect(result).toBe(5);
  });
});

describe("Wrapper", () => {
  it("should create an instance", () => {
    const wrapper = new Wrapper("foo");
    expect(wrapper).toBeInstanceOf(Wrapper);
    expect(wrapper._value).toBe("foo");
  });

  it("should implement a map function", () => {
    const wrapper = new Wrapper(2);
    const result = wrapper.map(x => x * 2);
    expect(result._value).toBe(4);
  });

  it("should obey the identity law", () => {
    const wrapper = new Wrapper("foo");
    const result = wrapper.map(identity);
    expect(result._value).toBe("foo");
  });

  it("should obey the composition law", () => {
    const wrapper = new Wrapper(2);

    const f = x => x * 2;
    const g = x => x + 1;
    const result1 = wrapper.map(value => f(g(value)));
    const result2 = wrapper.map(g).map(f);

    expect(result1._value).toBe(result2._value);
  });
});
