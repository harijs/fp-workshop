import containsDeep from "./containsDeep";

describe("containsDeep", () => {
  const myObj = {
    somekey: ["foo", "bar"],
    otherkey: "baz",
    another: 7,
    someobj: {
      morekeys: ["bag", "faz"],
      deeper: {
        more: "yes"
      },
      bool: false
    }
  };

  it("it should find 'foo'", () => {
    const result = containsDeep("foo")(myObj);
    expect(result).toBe(true);
  });

  it("it should not find 'foobar'", () => {
    const result = containsDeep("foobar")(myObj);
    expect(result).toBe(false);
  });

  it("it should find false", () => {
    const result = containsDeep(false)(myObj);
    expect(result).toBe(true);
  });

  it("it should find 7", () => {
    const result = containsDeep(7)(myObj);
    expect(result).toBe(true);
  });

  it("it should find 'faz'", () => {
    const result = containsDeep("faz")(myObj);
    expect(result).toBe(true);
  });

  it("it should not find 'no'", () => {
    const result = containsDeep("no")(myObj);
    expect(result).toBe(false);
  });

  it("it should find 'yes'", () => {
    const result = containsDeep("yes")(myObj);
    expect(result).toBe(true);
  });
});
