import getPassionatePeople, { isPassionatePerson } from "./getPassionatePeople";

describe("getPassionatePeople", () => {
  it("should return passionate people", () => {
    const people = [
      {
        name: "Action Hank",
        isPassionate: true
      },
      {
        name: "Dee-Dee",
        isPassionate: false
      }
    ];

    const spy = jest.spyOn(people, "filter");

    const passionatePeople = getPassionatePeople(people);

    expect(passionatePeople).toEqual([
      {
        name: "Action Hank",
        isPassionate: true
      }
    ]);
    expect(spy).toHaveBeenCalledWith(isPassionatePerson);
  });
});
