# Passionate People - Functional Programming Workshop

## Installation

Please run `npm install` to install all dependencies.

The dependencies also include one popular FP library (Ramda) in case you want to use a utility function such as `curry` and `compose`. You can of course install another one of your choosing, such as Sanctuary or Crocks. 🙂

## Assignments

All assignments are covered with Jest tests. You can run the different assignments with the NPM scripts listed below.
Every test has a corresponding `README.md` inside the same directory with some extra information and hints.

Jest will guide you in everything that is breaking, but the workflow is as follows:
1) run the script for the assignment (this will run a Jest test)
2) see everything break
3) create the file that should contain the solution
4) read the corresponding `README.md`
5) fix the tests!

The tests are written in such a way that every single function is expected to be curried (🍛) to get you familiar with the concept (if you're not already). 

You can choose to:
 - run your function through the `curry` utility function of your favourite library
 - curry them yourself by return functions inside functions inside functions inside functions ..
 - write your own `curry` utility function! 🤓

| Assignment     | Script                                   |
| -------------- | ---------------------------------------- |
| Warm-up 1      | `npm run assignment:warmup:contains`     |
| Warm-up 2      | `npm run assignment:warmup:containsDeep` |
| Immutability   | `npm run assignment:immutability`        |
| Point-free     | `npm run assignment:point-free`          |
| Currying       | `npm run assignment:currying`            |
| Composition    | `npm run assignment:composition`         |
| Transducers    | `npm run assignment:transducer`          |
| Right Angles   | `npm run assignment:rightAngles`         |
| Functors       | `npm run assignment:functor`             |
| Maybe Monad    | `npm run assignment:monads:maybe`        |
| Run all tests! | `npm run test`                           |
