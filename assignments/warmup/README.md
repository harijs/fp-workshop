# Warmup

## contains

`contains :: a -> Object -> Bool`

Write a utility function in functional style that traverses an object and finds if one of its keys or an array holds a certain value.

Feel free to use built in Array.{map, reduce, some, every, includes, flat… etc} and Object.{keys, entries, values… etc} methods. As well as spread and rest syntax if necessary.

---

## containsDeep

`containsDeep :: a -> Object -> Bool`

Same idea, just this time object is deeply nested and your function should search all the way through.

Feel free to use built in Array.{map, reduce, some, every, includes, flat… etc} and Object.{keys, entries, values… etc} methods. As well as spread and rest syntax if necessary.

### Hints

- You might need a util function for your util function

---