import updateItem from "./updateItem";

describe("updateItem", () => {
  it("should update an item in the array without mutating the original", () => {
    const originalArray = [1, 2, 3, 4];

    const updateItemInArray = updateItem(originalArray);
    const updateIndexWithNewValue = updateItemInArray(1);
    const newArray = updateIndexWithNewValue(-2);

    expect(originalArray).toEqual([1, 2, 3, 4]);
    expect(newArray).toEqual([1, -2, 3, 4]);
  });

  it("should keep the original array intact when an item is not found", () => {
    const originalArray = [1, 2, 3, 4];

    const newArray = updateItem(originalArray)(5)(-1);

    expect(originalArray).toEqual([1, 2, 3, 4]);
    expect(newArray).toEqual([1, 2, 3, 4]);
  });
});
