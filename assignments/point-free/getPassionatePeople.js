export const isPassionatePerson = person => person.isPassionate;

const getPassionatePeople = people =>
  people.filter(person => isPassionatePerson(person));

export default getPassionatePeople;
