# Immutability

## appendItem

The function accepts two arguments. The first is the array on which the new item must be appended to. The second is the item that is being appended.  It returns a new array and keeps the original intact.

`appendItem :: Array a -> a -> Array a`

## prependItem

The function accepts two arguments. The first is the array on which the new item must be prepended to. The second is the item that is being prepended.  It returns a new array and keeps the original intact.

`prependItem :: Array a -> a -> Array a`

## removeItem

The function accepts two arguments. The first is the array on which the item must be removed from. The second is the item that is being removed. It returns a new array and keeps the original intact.

`removeItem :: Array a -> a -> Array a`

## updateItem

 The function accepts three arguments. The first is the array on which the item must be updated. The second is the index of the item that is being updated. The third is the new value of the item. It returns a new array and keeps the original intact.

`updateItem :: Array a -> Number -> a -> Array a`