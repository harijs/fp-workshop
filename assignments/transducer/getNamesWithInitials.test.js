import names from "./names.json";
import getNamesWithInitials from "./getNamesWithInitials";

describe("getNamesWithInitials", () => {
  it("it should return only names with provided initials", () => {
    const result = getNamesWithInitials(["FP", "JS", "WTF", "LOL"])(names);
    expect(result).toMatchSnapshot();
  });
});
