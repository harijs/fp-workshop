import prependItem from "./prependItem";

describe("prependItem", () => {
  it("should prepend an item to the array without mutating the original", () => {
    const originalArray = [2, 3, 4];

    const prependToArray = prependItem(originalArray);
    const newArray = prependToArray(1);

    expect(originalArray).toEqual([2, 3, 4]);
    expect(newArray).toEqual([1, 2, 3, 4]);
  });
});
