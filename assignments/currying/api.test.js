import api from "./api";

describe("api", () => {
  it("should create a base api helper with one argument", () => {
    const froppelApi = api("https://api.froppel.com/v1");
    expect(froppelApi).toBeInstanceOf(Function);
    expect(froppelApi.length).toBe(1);
  });

  it("should create a base GET helper with one argument", () => {
    const get = api("https://api.froppel.com/v1")("GET");
    expect(get).toBeInstanceOf(Function);
    expect(get.length).toBe(1);
  });

  it("should create a base POST helper with one argument", () => {
    const post = api("https://api.froppel.com/v1")("POST");
    expect(post).toBeInstanceOf(Function);
    expect(post.length).toBe(1);
  });

  it("should create an base url helper", () => {
    const getFroppels = api("https://api.froppel.com/v1")("GET")("/froppels");
    expect(getFroppels).toBeInstanceOf(Function);

    const result = getFroppels();
    expect(getFroppels()).resolves.toEqual([
      {
        id: 1,
        title: "I love froppels"
      }
    ]);
  });
});
