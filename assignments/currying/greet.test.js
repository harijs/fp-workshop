import greet from "./greet";

describe("hello", () => {
  it("should greet the functional programmer", () => {
    const sayHello = greet("Hello");
    const result = sayHello("functional programmer");

    expect(result).toBe("Hello, functional programmer!");
  });
});
