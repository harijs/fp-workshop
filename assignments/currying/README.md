# Currying

## add

This function accepts a number as the first argument and it returns a function. That function also accepts a number and will return the sum of the two numbers.

`add :: Number -> Number -> Number`

## greet

This function accepts a string (the greeter) as the first argument and it returns a function. That function also accepts a string (name) and will return a greeting message.

`greet :: String -> String -> String`

## api

This function accepts three arguments. The first is a baseUrl, the second is the method (GET, POST, etc.) and the third is the actual path. This function returns a Promise.

`api :: String -> String -> String -> Promise`