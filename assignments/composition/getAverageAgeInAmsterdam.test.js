import passionatePeople from "./passionatePeople.json";
import {
  filter,
  map,
  prop,
  equals,
  getCity,
  getAge,
  getAverageNumber,
  isAmsterdam,
  arrayLength,
  arraySum,
  divide,
  round,
  fork,
  personLivesInAmsterdam,
  filterOnPeopleFromAmsterdam,
  getAverageAgeInAmsterdam
} from "./getAverageAgeInAmsterdam";

const isOdd = x => x % 2;
const toUpperCase = val => val.toUpperCase();
const toLowerCase = val => val.toLowerCase();

describe("getAverageAgeInCity", () => {
  describe("filter", () => {
    it("should be defined", () => {
      expect(filter).toBeDefined();
    });

    it("should filter the array", () => {
      const originalArray = [1, 2, 3, 4];

      const filterOdd = filter(isOdd);
      const newArray = filterOdd(originalArray);

      expect(originalArray).toEqual([1, 2, 3, 4]);
      expect(newArray).toEqual([1, 3]);
    });
  });

  describe("map", () => {
    it("should be defined", () => {
      expect(map).toBeDefined();
    });

    it("should map the array", () => {
      const originalArray = ["a", "b", "c"];

      const toUpperCase = val => val.toUpperCase();
      const mapToUpper = map(toUpperCase);
      const newArray = mapToUpper(originalArray);

      expect(originalArray).toEqual(["a", "b", "c"]);
      expect(newArray).toEqual(["A", "B", "C"]);
    });
  });

  describe("prop", () => {
    it("should be defined", () => {
      expect(prop).toBeDefined();
    });

    it("should return the value inside an object given a key", () => {
      const key = "foo";
      const object = { foo: "bar" };

      const getFoo = prop("foo");
      const result = getFoo(object);

      expect(result).toBe("bar");
    });
  });

  describe("equals", () => {
    it("should be defined", () => {
      expect(equals).toBeDefined();
    });

    it("should strict equal", () => {
      const isTrue = equals(true);
      const isLowercaseA = equals("a");
      const isNumberOne = equals(1);

      expect(isTrue(true)).toBe(true);
      expect(isLowercaseA("a")).toBe(true);
      expect(isNumberOne("1")).toBe(false);
    });
  });

  describe("getCity", () => {
    it("should be defined", () => {
      expect(getCity).toBeDefined();
    });

    it("should get the city", () => {
      expect(
        getCity({
          city: "Foo City"
        })
      ).toBe("Foo City");
    });
  });

  describe("getAge", () => {
    it("should be defined", () => {
      expect(getAge).toBeDefined();
    });

    it("should get the age", () => {
      expect(
        getAge({
          age: 28
        })
      ).toBe(28);
    });
  });

  describe("getAverageNumber", () => {
    it("should be defined", () => {
      expect(getAverageNumber).toBeDefined();
    });

    it("should return a rounded average on a list of numbers", () => {
      expect(getAverageNumber([21, 453, 21, 34, 32, 345])).toBe(151);
    });
  });

  describe("isAmsterdam", () => {
    it("should be defined", () => {
      expect(isAmsterdam).toBeDefined();
    });

    it("should check if the value equals 'Amsterdam'", () => {
      expect(isAmsterdam("Amsterdam")).toBe(true);
    });
  });

  describe("arrayLength", () => {
    it("should be defined", () => {
      expect(arrayLength).toBeDefined();
    });

    it("should return the length of the array", () => {
      expect(arrayLength([1, 2, 3, 4, 5])).toBe(5);
    });
  });

  describe("arraySum", () => {
    it("should be defined", () => {
      expect(arraySum).toBeDefined();
    });

    it("should sum all the items in the array", () => {
      expect(arraySum([1, 2, 3, 4, 5])).toBe(15);
    });
  });

  describe("divide", () => {
    it("should be defined", () => {
      expect(divide).toBeDefined();
    });

    it("should divide two numbers", () => {
      expect(divide(20)(5)).toBe(4);
    });
  });

  describe("round", () => {
    it("should be defined", () => {
      expect(round).toBeDefined();
    });

    it("should round the number", () => {
      expect(round(10.2)).toBe(10);
      expect(round(10.5)).toBe(11);
    });
  });

  describe("fork", () => {
    it("should be defined", () => {
      expect(fork).toBeDefined();
    });

    it("should implement the fork combinator", () => {
      const join = result1 => result2 => `${result1}_${result2}`;
      const myFork = fork(join)(toLowerCase)(toUpperCase);
      const result = myFork("SomEtext");
      expect(result).toBe("sometext_SOMETEXT");
    });
  });

  describe("personLivesInAmsterdam", () => {
    it("should be defined", () => {
      expect(personLivesInAmsterdam).toBeDefined();
    });

    it("should return true if the person lives in Amsterdam", () => {
      expect(
        personLivesInAmsterdam({
          city: "Amsterdam"
        })
      ).toBe(true);
    });

    it("should return false if the person does not live in Amsterdam", () => {
      expect(
        personLivesInAmsterdam({
          city: "Utrecht"
        })
      ).toBe(false);
    });
  });

  describe("filterOnPeopleFromAmsterdam", () => {
    it("should be defined", () => {
      expect(filterOnPeopleFromAmsterdam).toBeDefined();
    });

    it("should return only people from Amsterdam", () => {
      const peopleInAmsterdam = filterOnPeopleFromAmsterdam([
        { city: "Amsterdam" },
        { city: "Utrecht" },
        { city: "Diemen" }
      ]);
      expect(peopleInAmsterdam).toEqual([{ city: "Amsterdam" }]);
    });
  });

  describe("getAverageAgeInAmsterdam", () => {
    it("should be defined", () => {
      expect(getAverageAgeInAmsterdam).toBeDefined();
    });

    it("it should return the rounded average age from people in Amsterdam", () => {
      const averageAge = getAverageAgeInAmsterdam(passionatePeople);

      expect(averageAge).toBe(28);
    });
  });
});
