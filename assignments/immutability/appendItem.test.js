import appendItem from "./appendItem";

describe("appendItem", () => {
  it("should append an item to the array without mutating the original", () => {
    const originalArray = [1, 2, 3, 4];

    const appendToArray = appendItem(originalArray);
    const newArray = appendToArray(5);

    expect(originalArray).toEqual([1, 2, 3, 4]);
    expect(newArray).toEqual([1, 2, 3, 4, 5]);
  });
});
