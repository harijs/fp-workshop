# Point-free

## getPassionatePeople

`getPassionatePeople :: Array Person -> Array Person`

Make sure that the test passes by making the implementation point-free.

---

## map

`map :: (a -> b) -> Array a -> Array b`

Make sure that the test passes by making the implementation point-free.