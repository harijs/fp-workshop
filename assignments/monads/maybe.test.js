import R from "ramda";
import {
  Maybe,
  Just,
  Nothing,
  safeProp,
  safeName,
  safeToUpperCase,
  getPersonNameInUpperCase
} from "./maybe";

describe("Maybe", () => {
  it("should be defined", () => {
    expect(Maybe).toBeDefined();
  });

  it("should have a static shorthand", () => {
    const maybe = Maybe.of("foo");
    expect(Maybe.of).toBeInstanceOf(Function);
    expect(maybe).toBeInstanceOf(Maybe);
    expect(maybe._value).toBe("foo");
  });

  describe("Just", () => {
    it("should be defined", () => {
      expect(Just).toBeDefined();
    });

    it("should be of subtype Just using `Maybe.of` with a truthy value", () => {
      const maybe = Maybe.of("foo");
      expect(maybe).toBeInstanceOf(Just);
      expect(maybe.isJust).toBe(true);
      expect(maybe.isNothing).toBe(false);
    });

    it("should be of Just subtype", () => {
      const maybe = Maybe.just("foo");
      expect(maybe).toBeInstanceOf(Just);
      expect(maybe.isJust).toBe(true);
      expect(maybe.isNothing).toBe(false);
    });

    it("should be have a debug method", () => {
      const maybe = Maybe.just("foo");
      expect(maybe.toString()).toBe("Just(foo)");
    });

    it("should obey the identity law", () => {
      const maybe = Maybe.just("foo");
      const result = maybe.map(x => x);
      expect(result.toString()).toBe("Just(foo)");
    });

    it("should obey the composition law", () => {
      const maybe = Maybe.just("foo");
      const result = maybe.map(s => s.toUpperCase());
      expect(result).toBeInstanceOf(Just);
      expect(result.toString()).toBe("Just(FOO)");
    });

    it("should have a join method to remove nested Just's", () => {
      const maybe = Maybe.just(Maybe.just("foo"));
      expect(maybe.toString()).toBe("Just(Just(foo))");
      const result = maybe.join();
      expect(result.toString()).toBe("Just(foo)");
    });

    it("should return the same value if it is not nested", () => {
      const maybe = Maybe.just("foo");
      const result = maybe.join();
      expect(result.toString()).toBe("Just(foo)");
    });

    it("should have a chain method to apply map + chain without nested Just's", () => {
      const maybe = Maybe.just("foo");
      const result = maybe.chain(x => Maybe.just("bar"));
      expect(result.toString()).toBe("Just(bar)");
    });
  });

  describe("Nothing", () => {
    it("should be defined", () => {
      expect(Nothing).toBeDefined();
    });

    it("should be of subtype Nothing when using `Maybe.of` with a falsy value", () => {
      const maybe = Maybe.of(null);
      expect(maybe).toBeInstanceOf(Nothing);
      expect(maybe.isNothing).toBe(true);
      expect(maybe.isJust).toBe(false);
    });

    it("should be of Nothing subtype", () => {
      const maybe = Maybe.just("foo");
      expect(maybe).toBeInstanceOf(Just);
      expect(maybe.isJust).toBe(true);
      expect(maybe.isNothing).toBe(false);
    });

    it("should be have a debug method", () => {
      const maybe = Maybe.nothing();
      expect(maybe.toString()).toBe("Nothing");
    });

    it("should obey the identity law", () => {
      const maybe = Maybe.nothing();
      const result = maybe.map(x => x);
      expect(result.toString()).toBe("Nothing");
    });

    it("should obey the composition law", () => {
      const maybe = Maybe.nothing();
      const result = maybe.map(s => s.toUpperCase());
      expect(result).toBeInstanceOf(Nothing);
      expect(result.toString()).toBe("Nothing");
    });

    it("should never have nested Nothing's", () => {
      const maybe = Maybe.nothing(Maybe.nothing());
      expect(maybe.toString()).toBe("Nothing");
    });

    it("should have a chain method", () => {
      const maybe = Maybe.nothing(Maybe.nothing());
      const result = maybe.chain(x => x);
      expect(result.toString()).toBe("Nothing");
    });

    it("should have a join method", () => {
      const maybe = Maybe.nothing(Maybe.nothing());
      const result = maybe.join();
      expect(result.toString()).toBe("Nothing");
    });
  });

  describe("safeProp", () => {
    it("should return a Just type if it exists", () => {
      const person = {
        name: "Action Hank"
      };

      const name = safeProp("name")(person);

      expect(name).toBeInstanceOf(Just);
      expect(name.toString()).toBe("Just(Action Hank)");
    });

    it("should return a Nothing type if object is falsy", () => {
      const name = safeProp("name")(null);

      expect(name).toBeInstanceOf(Nothing);
    });

    it("should return a Nothing if it does not exist", () => {
      const person = {
        name: "Action Hank"
      };

      const name = safeProp("age")(person);

      expect(name).toBeInstanceOf(Nothing);
    });
  });

  describe("composition", () => {
    describe("safeToUpperCase", () => {
      it("should be defined", () => {
        expect(safeToUpperCase).toBeDefined();
      });

      it("should return a Just", () => {
        const result = safeToUpperCase("foo");
        expect(result).toBeInstanceOf(Just);
        expect(result.toString()).toBe("Just(FOO)");
      });

      it("should return a Nothing", () => {
        const result = safeToUpperCase();
        expect(result).toBeInstanceOf(Nothing);
      });
    });

    describe("safeName", () => {
      it("should be defined", () => {
        expect(safeName).toBeDefined();
      });

      it("should return a Just", () => {
        const result = safeName({ name: "Hank" });
        expect(result).toBeInstanceOf(Just);
      });

      it("should return a Nothing", () => {
        const result = safeName({ age: 26 });
        expect(result).toBeInstanceOf(Nothing);
      });
    });

    describe("getPersonNameInUpperCase", () => {
      it("should be defined", () => {
        expect(getPersonNameInUpperCase).toBeDefined();
      });

      it("should compose with a valid value", () => {
        const person = {
          name: "Action Hank"
        };

        const name = getPersonNameInUpperCase(person);

        expect(name).toBeInstanceOf(Just);
        expect(name.toString()).toBe("Just(ACTION HANK)");
      });

      it("should compose with an invalid value", () => {
        const person = {
          age: 27
        };

        const name = getPersonNameInUpperCase(person);

        expect(name).toBeInstanceOf(Nothing);
      });
    });
  });
});
