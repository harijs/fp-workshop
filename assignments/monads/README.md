# Monads

## Maybe

We're going to write our own implementation of the `Maybe` monad, because why not?

First we must make sure that the implementation of the `Maybe`  monad passes. When that is done, we'll be using it in a simple example and in a composition.

Once we get to the composition, we need the following functions.

### safeProp

`safeProp :: String -> Object -> Maybe a`

### toUpperCase

`toUpperCase :: String -> Maybe String`

### safeName

`safeName :: Object -> Maybe a`

### getPersonNameInUpperCase

`getPersonNameInUpperCase :: Object -> Maybe String`

This function is a composition of `safeName` and `toUpperCase`. Remember that you must `map` over `safeUpperCase` in order to morph the original value into a new value!

```
|> safeToUpperCase :: Maybe String
|> safeName :: Object -> Maybe a
```
