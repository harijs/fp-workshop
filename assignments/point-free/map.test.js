import map, { toUpper } from "./map";

describe("map", () => {
  it("should change all names to uppercase", () => {
    const names = [
      "Action Hank",
      "Dee Dee",
      "Dexter",
      "Mom",
      "Dad",
      "Mandark",
      "Monkey"
    ];

    const spy = jest.spyOn(names, "map");
    const mapToUpper = map(toUpper);
    const uppercaseNames = mapToUpper(names);

    expect(uppercaseNames).toEqual([
      "ACTION HANK",
      "DEE DEE",
      "DEXTER",
      "MOM",
      "DAD",
      "MANDARK",
      "MONKEY"
    ]);
    expect(spy).toHaveBeenCalledWith(toUpper);
  });
});
