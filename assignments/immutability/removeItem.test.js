import removeItem from "./removeItem";

describe("removeItem", () => {
  it("should remove an item to the array without mutating the original", () => {
    const originalArray = [1, 2, 3, 4];

    const removeItemFromArray = removeItem(originalArray);
    const newArray = removeItemFromArray(2);

    expect(originalArray).toEqual([1, 2, 3, 4]);
    expect(newArray).toEqual([1, 3, 4]);
  });

  it("should keep the original array intact when an item is not found", () => {
    const originalArray = [1, 2, 3, 4];

    const removeItemFromArray = removeItem(originalArray);
    const newArray = removeItemFromArray(5);

    expect(originalArray).toEqual([1, 2, 3, 4]);
    expect(newArray).toEqual([1, 2, 3, 4]);
  });
});
