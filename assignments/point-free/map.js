export const toUpper = char => char.toUpperCase();

const map = fn => names => names.map(name => fn(name));

export default map;
